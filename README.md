# pccds-poc

This project was developed for the [2019 Annual PCCDS Conference](https://pccds-ln.org/2019conference).

pccds-poc is a proof-of-concept Patient-facing
[SMART on FHIR](https://docs.smarthealthit.org) application that will pull a given
patient's [DSTU2 FHIR](http://hl7.org/fhir/)
[Patient](http://hl7.org/fhir/DSTU2/patient.html) and
[MedicationOrder](http://hl7.org/fhir/DSTU2/medicationorder.html) resource in order
to display the amount of opioids (Morphine Equivalent) the patient is taking and
provide a basic recommendation regarding the patient's opioid use.  The application
is configured to connect to a specific opioid-mme service that implements the
[CDS Hooks](https://cds-hooks.hl7.org) standard.

## Requirements
To run the project, there must be an OAuth 2.0 endpoint for application authorization
(and possibly patient login) and a dedicated FHIR server for data access.
A proof-of-concept implementation of an OAuth 2.0 and FHIR service
([fhir-proxy](https://bitbucket.org/euvitudo/fhir-proxy/)) may be used.

See ```src/config.js``` for configuration details.

## Setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

This will run the application at the following endpoint:
- `http://localhost:4545/pccds`

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

## License

This project is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0).