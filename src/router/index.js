import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Authorize from '@/components/Authorize'
import Redirect from '@/components/Redirect'
import Opioid from '@/components/Opioid'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: '/pccds',
    routes: [
        {
            path: '/',
            name: 'main',
            component: Main,
            props: true
        },
        {
            path: '/authorize',
            name: 'authorize',
            component: Authorize,
            props: true
        },
        {
            path: '/redirect',
            name: 'redirect',
            component: Redirect,
            props: true
        },
        {
            path: '/opioid',
            name: 'opioid',
            component: Opioid,
            props: true
        }
    ]
})
