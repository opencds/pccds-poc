/*
Copyright 2020 OpenCDS.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
export default {
  base_url: 'http://localhost:8080/fhir-proxy/api/DSTU2',
  conformance_url: 'http://localhost:8080/fhir-proxy/api/dstu2/metadata',
  cds_url: 'https://fhir.cygni.cc/opencds/hooks/cds-services/opioid-mme-sof',
  client_id: 'pccds-client',
  redirect_uri: 'http://localhost:4545/pccds/redirect',
  scope: 'resources:read'
}
